import os
import json

from sanic import Sanic
from sanic.response import json as sjson
from sanic.exceptions import abort


POST_RESPONSE_SUCCESS_CREATE_INCEDENT = {'hello': 'test'}


with open('pd_resp_success.json', 'r') as f:
    POST_RESPONSE_SUCCESS_CREATE_INCEDENT = json.loads(f.read())


app = Sanic(__name__)


@app.route('/incidents', methods=['GET', 'POST'])
async def test(request):

    if request.headers.get('Authorization') != f'Token token={os.getenv("API_KEY")}':
        abort(401)
    if request.headers.get('From') != f'{os.getenv("EMAIL")}':
        abort(400, message="EMAL not recognized")
    if request.json['incident']['service']['id'] != f'{os.getenv("SERVICE_ID")}':
        abort(400, message="Service id must be a valid ID.")

    return sjson(POST_RESPONSE_SUCCESS_CREATE_INCEDENT, status=201)


if __name__ == '__main__':
    ssl = {'cert': "certificate.pem", 'key': "key.pem"}
    app.run(host='0.0.0.0', port=443, debug=True, ssl=ssl)
