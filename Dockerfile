FROM python:3.7-slim

COPY requirements.txt /usr/bin
COPY pipe.yml LICENSE.txt README.md /usr/bin/

WORKDIR /usr/bin
COPY pipe /usr/bin/

RUN pip install -r requirements.txt

ENTRYPOINT ["python3", "/usr/bin/pipe.py"]
