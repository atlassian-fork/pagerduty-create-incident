import os

import yaml
import requests

from bitbucket_pipes_toolkit import Pipe, get_logger


BASE_URL = 'https://api.pagerduty.com'
INCIDENT_URL = f'{BASE_URL}/incidents'
BASE_FAILED_MESSAGE = 'Failed to create an incident in PagerDuty'
BASE_TEXT = 'Incident triggered from Bitbucket Pipelines'

workspace = os.getenv('BITBUCKET_WORKSPACE', 'local')
repo = os.getenv('BITBUCKET_REPO_SLUG', 'local')
build = os.getenv('BITBUCKET_BUILD_NUMBER', 'local')

DEFAULT_PIPELINES_URL = f'https://bitbucket.org/{workspace}/{repo}/addon/pipelines/home#!/results/{build}'
URGENCY_ALLOWED_VALUES = ["high", "low"]


class NotValidVariable(Exception):
    pass


logger = get_logger()


schema = {
    "API_KEY": {
        "type": "string",
        "required": True
    },
    "EMAIL": {
        "type": "string",
        "required": True
    },
    "SERVICE_ID": {
        "type": "string",
        "required": True
    },
    "URGENCY": {
        "type": "string",
        "default": "high",
        "allowed": URGENCY_ALLOWED_VALUES
    },
    "INCIDENT_TITLE": {
        "type": "string",
        "default": BASE_TEXT
    },
    "DESCRIPTION": {
        "type": "string",
        "default": BASE_TEXT
    },
    "CLIENT_URL": {
        "type": "string",
        "default": DEFAULT_PIPELINES_URL
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    }
}


class PagerDutyCreateIncidetPipe(Pipe):

    def run(self):
        api_key = self.get_variable('API_KEY')
        email = self.get_variable('EMAIL')
        service_id = self.get_variable('SERVICE_ID')

        urgency = self.get_variable('URGENCY').lower()
        incident_title = self.get_variable('INCIDENT_TITLE')
        incident_details = self.get_variable('DESCRIPTION')

        client_url = self.get_variable('CLIENT_URL')
        incident_details = " ".join([incident_details, client_url])
        debug = self.get_variable('DEBUG')

        header = {
            'Content-Type': 'application/json',
            'Authorization': f'Token token={api_key}',
            'Accept': 'application/vnd.pagerduty+json;version=2',
            'From': email
        }

        payload = {
            "incident": {
                "type": "incident",
                "title": incident_title,
                "service": {
                    "id": service_id,
                    "type": "service_reference"
                },
                "urgency": urgency,
                "body": {
                    "type": "incident_body",
                    "details": incident_details
                }
            }
        }

        logger.info('Creating an incident in PagerDuty...')

        try:
            response = requests.post(
                INCIDENT_URL,
                headers=header,
                json=payload,
            )
        except requests.exceptions.Timeout as e:
            message = '{}: {}{}'.format(
                BASE_FAILED_MESSAGE,
                'Request to PagerDuty timed out',
                f': {e}' if debug == 'true' else f'.'
            )
            self.fail(message=message)
        except requests.ConnectionError as e:
            message = '{}: {}{}'.format(
                BASE_FAILED_MESSAGE,
                'Connection Error',
                f': {e}' if debug == 'true' else f'.'
            )
            self.fail(message=message)

        if response.status_code == 401:
            self.fail(message=(
                f'{BASE_FAILED_MESSAGE}: '
                f'You did not supply credentials or did not provide the correct '
                f'credentials. If you are using an API key, it may be invalid or '
                f'malformed.')
            )
        elif response.status_code != 201:
            self.fail(message=f'{BASE_FAILED_MESSAGE}: {response.text}')

        self.success(f'Incident successfully created. '
                     f'View incident: {response.json()["incident"]["html_url"]}')


if __name__ == '__main__':
    with open('/usr/bin/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())

    pipe = PagerDutyCreateIncidetPipe(pipe_metadata=metadata, schema=schema,
                                      check_for_newer_version=False)

    pipe.run()
