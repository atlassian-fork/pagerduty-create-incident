# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.4

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.2.3

- patch: Add warning message about new version of the pipe available.

## 0.2.2

- patch: Added code style checks

## 0.2.1

- patch: Internal maintenance: update pipes toolkit version

## 0.2.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.1.2

- patch: Minor documentation updates

## 0.1.1

- patch: Version bump

## 0.1.0

- minor: Initial release

